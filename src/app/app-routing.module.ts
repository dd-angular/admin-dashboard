import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { AuthRoutingModule } from './auth/auth.routing'
import { PagesRoutingModule } from './pages/pages.routing'

import { NotPagefoundComponent } from './notpagefound/notpagefound.component'

const routes: Routes = [
  {
    path: '**',
    component: NotPagefoundComponent,
  },
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    PagesRoutingModule,
    AuthRoutingModule,
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
