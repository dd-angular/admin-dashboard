import { Component, Input, OnInit } from '@angular/core'
import { ChartData, ChartType } from 'chart.js'

@Component({
  selector: 'app-doughnut',
  templateUrl: './doughnut.component.html',
  styles: [],
})
export class DoughnutComponent implements OnInit {
  @Input('labels') labels: string[] = []
  @Input('data') data: number[] = []
  @Input('title') title: string = ''

  charOptions = {
    resposive: true,
    maintainAspectRatio: false,
  } // TODO: evaluar si es candidato a ser @Input()

  public doughnutChartData: ChartData<'doughnut'> = {
    labels: [],
    datasets: [],
  }

  public doughnutChartType: ChartType = 'doughnut'

  ngOnInit(): void {
    this.doughnutChartData.labels = this.labels
    this.doughnutChartData.datasets = [{ data: this.data }]
  }
}
