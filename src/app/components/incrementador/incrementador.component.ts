import { Component, EventEmitter, Input, Output } from '@angular/core'

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
  styles: [],
})
export class IncrementadorComponent {
  @Input('valor') progreso: number = 10
  @Input('class') btnClass: string = 'btn-primary'

  @Output() valorActualizado: EventEmitter<number> = new EventEmitter()

  cambiarValor(valor: number): void {
    if (this.progreso >= 100 && valor >= 0) {
      this.progreso = 100
    } else if (this.progreso <= 0 && valor < 0) {
      this.progreso = 0
    } else if (this.progreso < 0 && valor > 0) {
      this.progreso = valor
    } else {
      this.progreso += valor
    }

    this.valorActualizado.emit(this.progreso)
  }

  onChange(valor: number): void {
    if (valor >= 100) {
      this.progreso = 100
    } else if (valor <= 0) {
      this.progreso = 0
    } else {
      this.progreso = valor
    }
    this.valorActualizado.emit(this.progreso)
  }
}
