import { Component, OnDestroy } from '@angular/core'
import { ActivationEnd, Router } from '@angular/router'
import { Subscription } from 'rxjs'
import { filter, map } from 'rxjs/operators'

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styles: [],
})
export class BreadcrumbsComponent implements OnDestroy {
  public title: string = ''
  public getRouteTitleSubs$: Subscription

  constructor(private readonly router: Router) {
    this.getRouteTitleSubs$ = this.getRouteTitle().subscribe({
      next: ({ title }) => {
        this.title = title
        document.title = `AdminPro - ${title}`
      },
      error: console.error,
    })
  }

  getRouteTitle() {
    return this.router.events.pipe(
      filter((e): e is ActivationEnd => e instanceof ActivationEnd),
      filter((e: ActivationEnd) => e.snapshot.firstChild === null),
      map((event: ActivationEnd) => event.snapshot.data),
    )
  }

  ngOnDestroy(): void {
    this.getRouteTitleSubs$.unsubscribe()
  }
}
