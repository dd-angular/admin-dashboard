import { Component } from '@angular/core'

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.css'],
})
export class ProgressComponent {
  progeso1: number = 15
  progeso2: number = 25

  get getPorcentaje1(): string {
    return `${this.progeso1}%`
  }

  get getPorcentaje2(): string {
    return `${this.progeso2}%`
  }
}
