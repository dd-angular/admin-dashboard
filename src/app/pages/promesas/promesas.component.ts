import { Component, OnInit } from '@angular/core'
import { User } from '../../interfaces/users.interface'

@Component({
  selector: 'app-promesas',
  templateUrl: './promesas.component.html',
  styles: [],
})
export class PromesasComponent implements OnInit {
  ngOnInit(): void {
    this.getUsuarios()
      .then((users) => console.log(users))
      .catch((e) => console.log(e))
  }

  getUsuarios(): Promise<User[]> {
    return new Promise((resolve, reject) => {
      fetch('https://reqres.in/api/users')
        .then((res) => res.json())
        .then((body) => resolve(body.data))
        .catch((e) => reject(e))
    })
  }
}
