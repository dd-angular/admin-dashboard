import { Component, OnDestroy } from '@angular/core'
import { Observable, interval, Subscription } from 'rxjs'
import { filter, map, retry, take } from 'rxjs/operators'

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: [],
})
export class RxjsComponent implements OnDestroy {
  private intervalSubs$: Subscription

  constructor() {
    /* obs$.pipe(retry(2)).subscribe({
      next: (val) => console.log('Subs: ', val),
      error: (e) => console.warn(e),
      complete: () => console.info('Subscription ended'),
    }) */
    /*  obs$.subscribe(
      (val) => console.log('Subs: ', val),
      (e) => console.warn(e),
      () => console.info('Subscription ended'),
    ) */

    /* this.retornaObservable()
      .pipe(retry())
      .subscribe({
        next: (val) => console.log('Subs: ', val),
        error: (e) => console.warn(e),
        complete: () => console.info('Subscription ended'),
      }) */

    this.intervalSubs$ = this.retornaIntervalo().subscribe({
      next: console.log,
      error: console.warn,
      complete: () => console.info('Subscription ended'),
    })
  }

  retornaIntervalo(): Observable<number> {
    return interval(500).pipe(
      // take(10),
      map((v) => v + 1),
      filter((v) => v % 2 === 0),
    )
  }

  retornaObservable(): Observable<number> {
    let i = -1

    return new Observable<number>((observer) => {
      const intervalo = setInterval(() => {
        i++
        observer.next(i)

        if (i === 6) {
          clearInterval(intervalo)
          observer.complete()
        }

        if (i === 4) {
          observer.error('Invalid number')
        }
      }, 1000)
    })
  }

  ngOnDestroy(): void {
    this.intervalSubs$.unsubscribe()
  }
}
