import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root',
})
export class SidebarService {
  menu: any[] = [
    {
      title: 'Dashboard',
      icon: 'mdi mdi-gauge',
      children: [
        {
          title: 'Main',
          path: '/',
        },
        {
          title: 'ProgressBar',
          path: '/progress',
        },
        {
          title: 'Gráficas',
          path: '/grafica1',
        },
        {
          title: 'Promesas',
          path: '/promesas',
        },
        {
          title: 'RXJS',
          path: '/rxjs',
        },
      ],
    },
  ]

  constructor() {}
}
