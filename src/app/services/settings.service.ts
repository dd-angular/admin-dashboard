import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root',
})
export class SettingsService {
  private themeLinkStyle: Element = document.querySelector('#theme')!
  private defaultTheme: string = './assets/css/colors/green-dark.css'

  constructor() {
    const theme = localStorage.getItem('theme') || this.defaultTheme
    this.themeLinkStyle.setAttribute('href', theme)
  }

  changeTheme(theme: string): void {
    const newURL = `./assets/css/colors/${theme}.css`
    this.themeLinkStyle?.setAttribute('href', newURL)
    localStorage.setItem('theme', newURL)

    this.checkCurrentTheme()
  }

  checkCurrentTheme(): void {
    const themeItems: NodeListOf<Element> = document.querySelectorAll(
      '.selector',
    )

    themeItems.forEach((e) => {
      e.classList.remove('working')

      const btnTheme = e.getAttribute('data-theme')
      const btnThemeUrl = `./assets/css/colors/${btnTheme}.css`
      const currentTheme = this.themeLinkStyle.getAttribute('href')

      if (btnThemeUrl === currentTheme) {
        e.classList.add('working')
      }
    })
  }
}
